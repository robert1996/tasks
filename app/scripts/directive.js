'use strict';

angular.module('App').directive('addPost', function () {
  return {
    link: function (scope, element, attributes) {
    },
    restrict: 'A',
    templateUrl: 'views/post.html',
    scope: true
  };
});
