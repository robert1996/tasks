'use strict';

angular.module('App').factory('Post', [function () {
  var Post = function(title, link){
    this.title = title;
    this.link = link;
    this.votes = 0;
  };
  Post.prototype.upvote = function(){
    this.votes ++;
  };
  Post.prototype.downvote = function(){
    if(this.votes > 0){
      this.votes --;
    }
  };
  return Post;
}]);
