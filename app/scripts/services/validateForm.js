'use strict';

angular.module('App').service('validateForm', [function () {
  return {
    title: {
      required: true,
      minLength: 3,
      maxLength: 10,
      regExp: /^[a-zA-Z0-9]+([_\s\-]?[a-zA-Z0-9])*$/
    },
    link: {
      required: true,
      minLength: 10,
      maxLength: 30,
      regExp: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/
    }
  };
}]);
