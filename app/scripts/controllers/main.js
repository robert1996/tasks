'use strict';

angular.module('App').controller('MainCtrl', ['$scope', 'Posts', '$log', 'validateForm', 'Post',
  function ($scope, Posts, $log, validateForm, Post) {
    $scope.conditions = validateForm;
    $scope.show = false;
    $scope.posts = [];

    $scope.submitForm = function (isvalid) {
      if (isvalid) {
        var post = new Post($scope.title, $scope.link);
        $scope.posts.push(post);
        $scope.show = true;
      } else {
        console.log('inValid');
      }
    };
  }
]);
